# projects

## Reddit stream 
[reddit_stream.zip](reddit_stream.zip)
- webova aplikace, ktera zobrazuje data z ruznych subredditu
- umoznuje sledovat subreddity
- python subproces odstranuje stop words
- propojeni socket.io
- nodejs, react, grommet

## Bazarek
[bazarek - gitlab](https://gitlab.com/ondrejholik/bazarek)
- webova stranka, ktera ukazuje pomoci airtable jake zbozi je zrovna k dispozici
- AIRTABLE API
- scope.apply z angular controlleru zajistuje, aby se asynchronni data zobrazili i po nacteni stranky
- frontend v bulma.io frameworku
- do priste bych dal api klice do samostatneho souboru --> bezpecnost
- angular, bulma.io, css, html

## Maturitni prace
[maturitni projekt - github](https://github.com/ondrejholik/Maturitni_projekt)
- v maturitni praci jsem vytvoril projekt, ktery streamuje data z twitteru do webove aplikace a nasledne pracuje s daty - napr. vytvari graf a tabulku dle jazyka, ve kterem byl tweet vytvoren
- nodejs, express, pug, socket.io

